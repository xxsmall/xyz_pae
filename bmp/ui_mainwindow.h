/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QDoubleSpinBox *doubleSpinBox_longitude;
    QDoubleSpinBox *doubleSpinBox_latitude;
    QDoubleSpinBox *doubleSpinBox_height;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox_r;
    QLabel *label_5;
    QLabel *label_6;
    QDoubleSpinBox *doubleSpinBox_a;
    QDoubleSpinBox *doubleSpinBox_e;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QDoubleSpinBox *doubleSpinBox_x;
    QDoubleSpinBox *doubleSpinBox_y;
    QDoubleSpinBox *doubleSpinBox_z;
    QLabel *label_11;
    QLabel *label_12;
    QPushButton *pushButton_ToXYZ;
    QPushButton *pushButton_ToPAE;
    QLabel *label_13;
    QPushButton *pushButton;
    QTextEdit *textEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1292, 727);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        doubleSpinBox_longitude = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_longitude->setObjectName(QStringLiteral("doubleSpinBox_longitude"));
        doubleSpinBox_longitude->setGeometry(QRect(30, 60, 171, 22));
        doubleSpinBox_longitude->setDecimals(8);
        doubleSpinBox_longitude->setMinimum(-360);
        doubleSpinBox_longitude->setMaximum(360);
        doubleSpinBox_longitude->setValue(108.95);
        doubleSpinBox_latitude = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_latitude->setObjectName(QStringLiteral("doubleSpinBox_latitude"));
        doubleSpinBox_latitude->setGeometry(QRect(250, 60, 171, 22));
        doubleSpinBox_latitude->setDecimals(8);
        doubleSpinBox_latitude->setMinimum(-360);
        doubleSpinBox_latitude->setMaximum(360);
        doubleSpinBox_latitude->setValue(34.87);
        doubleSpinBox_height = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_height->setObjectName(QStringLiteral("doubleSpinBox_height"));
        doubleSpinBox_height->setGeometry(QRect(470, 60, 171, 22));
        doubleSpinBox_height->setDecimals(8);
        doubleSpinBox_height->setMinimum(-99999);
        doubleSpinBox_height->setMaximum(99999);
        doubleSpinBox_height->setSingleStep(1);
        doubleSpinBox_height->setValue(697);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 30, 81, 16));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(250, 30, 81, 16));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(470, 30, 81, 16));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(470, 140, 81, 16));
        doubleSpinBox_r = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_r->setObjectName(QStringLiteral("doubleSpinBox_r"));
        doubleSpinBox_r->setGeometry(QRect(470, 170, 171, 22));
        doubleSpinBox_r->setDecimals(8);
        doubleSpinBox_r->setMinimum(-1e+10);
        doubleSpinBox_r->setMaximum(1e+10);
        doubleSpinBox_r->setValue(2.28591e+06);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(250, 140, 81, 16));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 140, 81, 16));
        doubleSpinBox_a = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_a->setObjectName(QStringLiteral("doubleSpinBox_a"));
        doubleSpinBox_a->setGeometry(QRect(30, 170, 171, 22));
        doubleSpinBox_a->setDecimals(8);
        doubleSpinBox_a->setMaximum(360);
        doubleSpinBox_a->setValue(33.3346);
        doubleSpinBox_e = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_e->setObjectName(QStringLiteral("doubleSpinBox_e"));
        doubleSpinBox_e->setGeometry(QRect(250, 170, 171, 22));
        doubleSpinBox_e->setDecimals(8);
        doubleSpinBox_e->setMinimum(-90);
        doubleSpinBox_e->setMaximum(90);
        doubleSpinBox_e->setValue(4.1058);
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(280, 10, 81, 16));
        label_7->setStyleSheet(QStringLiteral("font: 75 12pt \"Arial\";"));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(270, 120, 81, 16));
        label_8->setStyleSheet(QStringLiteral("font: 75 12pt \"Arial\";"));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(280, 230, 81, 16));
        label_9->setStyleSheet(QStringLiteral("font: 75 12pt \"Arial\";"));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(260, 250, 81, 16));
        doubleSpinBox_x = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_x->setObjectName(QStringLiteral("doubleSpinBox_x"));
        doubleSpinBox_x->setGeometry(QRect(40, 280, 171, 22));
        doubleSpinBox_x->setDecimals(8);
        doubleSpinBox_x->setMinimum(-1e+08);
        doubleSpinBox_x->setMaximum(1e+08);
        doubleSpinBox_y = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_y->setObjectName(QStringLiteral("doubleSpinBox_y"));
        doubleSpinBox_y->setGeometry(QRect(260, 280, 171, 22));
        doubleSpinBox_y->setDecimals(8);
        doubleSpinBox_y->setMinimum(-1e+09);
        doubleSpinBox_y->setMaximum(1e+09);
        doubleSpinBox_z = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_z->setObjectName(QStringLiteral("doubleSpinBox_z"));
        doubleSpinBox_z->setGeometry(QRect(480, 280, 171, 22));
        doubleSpinBox_z->setDecimals(8);
        doubleSpinBox_z->setMinimum(-1e+10);
        doubleSpinBox_z->setMaximum(1e+10);
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(480, 250, 81, 16));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(40, 250, 81, 16));
        pushButton_ToXYZ = new QPushButton(centralWidget);
        pushButton_ToXYZ->setObjectName(QStringLiteral("pushButton_ToXYZ"));
        pushButton_ToXYZ->setGeometry(QRect(760, 100, 75, 23));
        pushButton_ToPAE = new QPushButton(centralWidget);
        pushButton_ToPAE->setObjectName(QStringLiteral("pushButton_ToPAE"));
        pushButton_ToPAE->setGeometry(QRect(760, 160, 75, 23));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(50, 370, 921, 101));
        label_13->setStyleSheet(QStringLiteral("font: 75 12pt \"Arial\";"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(760, 280, 75, 23));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(930, 30, 321, 301));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1292, 23));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label->setText(QApplication::translate("MainWindow", "\347\273\217\345\272\246", 0));
        label_2->setText(QApplication::translate("MainWindow", "\347\272\254\345\272\246", 0));
        label_3->setText(QApplication::translate("MainWindow", "\351\253\230\347\250\213", 0));
        label_4->setText(QApplication::translate("MainWindow", "\350\267\235\347\246\273", 0));
        label_5->setText(QApplication::translate("MainWindow", "\344\277\257\344\273\260", 0));
        label_6->setText(QApplication::translate("MainWindow", "\346\226\271\344\275\215", 0));
        label_7->setText(QApplication::translate("MainWindow", "\346\265\213\347\253\231\345\261\236\346\200\247", 0));
        label_8->setText(QApplication::translate("MainWindow", "\346\265\213\347\253\231\346\265\213\351\207\217\345\200\274", 0));
        label_9->setText(QApplication::translate("MainWindow", "\345\234\260\345\277\203\347\263\273\346\265\213\351\207\217\345\200\274", 0));
        label_10->setText(QApplication::translate("MainWindow", "y", 0));
        label_11->setText(QApplication::translate("MainWindow", "z", 0));
        label_12->setText(QApplication::translate("MainWindow", "x", 0));
        pushButton_ToXYZ->setText(QApplication::translate("MainWindow", "PAE to XYZ", 0));
        pushButton_ToPAE->setText(QApplication::translate("MainWindow", "XYZ to PAE", 0));
        label_13->setText(QApplication::translate("MainWindow", "2018 12 07 02 41 43.572     -2138671.149     -3916864.311      5283931.870    -4082.624797    -4234.640896    -4791.027825\n"
"2018 12 07 10 41   43.00000         2285907.13   33.3346    4.1058", 0));
        pushButton->setText(QApplication::translate("MainWindow", "PushButton", 0));
        textEdit->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\270\255\345\215\216\344\272\272\346\260\221\345\205\261\345\222\214\345\233\275</p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
