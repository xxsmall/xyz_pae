#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("PAE <--> XYZ");
    Matrix4d p;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_ToXYZ_clicked()
{
    double  ae = 6378140.0;
    double  ec_2 = 0.0066946;
    double  pi = 3.1415926535897932384626433832795;
    double  we = 360.0/86164.0 ; //地球自转角速度

    double  fai = ui->doubleSpinBox_latitude->value();  //纬度;
    double  lanmuda = ui->doubleSpinBox_longitude->value(); //经度
    //double  lanmuda= ui->doubleSpinBox_latitude->value();
    //double  fai  = ui->doubleSpinBox_longitude->value();
    double  H = ui->doubleSpinBox_height->value();

    fai = fai/180.0*pi;
    lanmuda = lanmuda/180.0*pi;
    qDebug()<<"fai   lanmuda   H";
    qDebug()<<QString::number(fai,'f',6)<<QString::number(lanmuda,'f',6)<<QString::number(H,'f',6);

    double P = ui->doubleSpinBox_r->value();  //距离
    double A = ui->doubleSpinBox_a->value();  //方位
    double E = ui->doubleSpinBox_e->value();  //俯仰
    A = A/180.0*pi;
    E = E/180.0*pi;


    double N = ae/(sqrt(1.0-ec_2*sin(fai)*sin(fai)));

    Vector3d digu_M; //测站位置在地固系中的坐标为
    digu_M(0,0) = (N+H)*cos(fai)*cos(lanmuda);
    digu_M(1,0) = (N+H)*cos(fai)*sin(lanmuda);
    digu_M(2,0) = (N*(1.0-ec_2)+H)*sin(fai);

    qDebug()<<digu_M(0,0)<<endl<<digu_M(1,0)<<endl<<digu_M(2,0)<<endl;
    Vector3d station_M; //测站测量量，A，E在测站直角坐标中的分量
    station_M(0,0) = P*sin(A)*cos(E);
    station_M(1,0) = P*cos(A)*cos(E);
    station_M(2,0) = P*sin(E);

    Matrix3d M = MatrixXd::Random(3,3);//测站直角坐标系到地固系的转换矩阵
    M(0,0) = -sin(lanmuda);
    M(0,1) = -cos(lanmuda)*sin(fai);
    M(0,2) = cos(fai)*cos(lanmuda);

    M(1,0) = cos(lanmuda);
    M(1,1) = -sin(lanmuda)*sin(fai);
    M(1,2) = sin(lanmuda)*cos(fai);

    M(2,0) = 0.0;
    M(2,1) = cos(fai);
    M(2,2) = sin(fai);

    cout<<"look M: "<<M<<M.transpose();
    Vector3d xyz_M ;
    xyz_M = M*station_M + digu_M ;

    ui->doubleSpinBox_x->setValue(xyz_M(0,0));
    ui->doubleSpinBox_y->setValue(xyz_M(1,0));
    ui->doubleSpinBox_z->setValue(xyz_M(2,0));
    qDebug()<<"cal::  "<<QString::number(xyz_M(0,0),'f',6)
           <<QString::number(xyz_M(1,0),'f',6)
           <<QString::number(xyz_M(2,0),'f',6)
          <<QString::number(P,'f',6)
         <<QString::number(N,'f',6);
}

void MainWindow::on_pushButton_ToPAE_clicked()
{
    double  ae = 6378140.0;
    double  ec_2 = 0.0066946;
    double  pi = 3.1415926535897932384626433832795;
    double  we = 360.0/86164.0 ; //地球自转角速度

    double  fai = ui->doubleSpinBox_latitude->value();  //纬度;
    double  lanmuda = ui->doubleSpinBox_longitude->value(); //经度
    //double  lanmuda= ui->doubleSpinBox_latitude->value();
    //double  fai  = ui->doubleSpinBox_longitude->value();
    double  H = ui->doubleSpinBox_height->value();

    fai = fai/180.0*pi;
    lanmuda = lanmuda/180.0*pi;
    qDebug()<<"fai   lanmuda   H";
    qDebug()<<QString::number(fai,'f',6)<<QString::number(lanmuda,'f',6)<<QString::number(H,'f',6);

    double P ;  //距离
    double A ;  //方位
    double E ;  //俯仰
//    A = A/180.0*pi;
//    E = E/180.0*pi;


    double N = ae/(sqrt(1.0-ec_2*sin(fai)*sin(fai)));

    Vector3d digu_M; //测站位置在地固系中的坐标为
    digu_M(0,0) = (N+H)*cos(fai)*cos(lanmuda);
    digu_M(1,0) = (N+H)*cos(fai)*sin(lanmuda);
    digu_M(2,0) = (N*(1.0-ec_2)+H)*sin(fai);

    qDebug()<<digu_M(0,0)<<endl<<digu_M(1,0)<<endl<<digu_M(2,0)<<endl;
    Vector3d station_M; //测站测量量，A，E在测站直角坐标中的分量
//    station_M(0,0) = P*sin(A)*cos(E);
//    station_M(1,0) = P*cos(A)*cos(E);
//    station_M(2,0) = P*sin(E);

    Matrix3d M = MatrixXd::Random(3,3);//测站直角坐标系到地固系的转换矩阵
    M(0,0) = -sin(lanmuda);
    M(0,1) = -cos(lanmuda)*sin(fai);
    M(0,2) = cos(fai)*cos(lanmuda);

    M(1,0) = cos(lanmuda);
    M(1,1) = -sin(lanmuda)*sin(fai);
    M(1,2) = sin(lanmuda)*cos(fai);

    M(2,0) = 0.0;
    M(2,1) = cos(fai);
    M(2,2) = sin(fai);

    cout<<"look M: "<<M<<M.transpose();
    Vector3d xyz_M ;
    xyz_M(0,0) = ui->doubleSpinBox_x->value();
    xyz_M(1,0) = ui->doubleSpinBox_y->value();
    xyz_M(2,0) = ui->doubleSpinBox_z->value();

    station_M = M.transpose()*(xyz_M - digu_M);

    P = sqrt(station_M(0,0)*station_M(0,0)+station_M(1,0)*station_M(1,0)+station_M(2,0)*station_M(2,0));

    E = (180.0/pi) *( asin(station_M(2,0)/P) );

    A = (180.0/pi) *( atan(station_M(0,0)/station_M(1,0)) );
    //xyz_M = M*station_M + digu_M ;

    ui->doubleSpinBox_r->setValue(P);
    ui->doubleSpinBox_a->setValue(A);
    ui->doubleSpinBox_e->setValue(E);
    qDebug()<<"station P A E::  "<<QString::number(P,'f',6)
           <<QString::number(A,'f',6)
           <<QString::number(E,'f',6);


}

void Insert_small_bmp_to_big_bmp(int insert_x, int insert_y, QString big_file, QString small_file,QString source_file)
{
    char *m_pbmpdata_90;
    char *pkl_90;
    BITMAPINFO Bitinfo_90;
    int x_tu_90,y_tu_90;
    int len_tu_90;
    int byte_of_90_xLine;
    int xLine_add_90;
    DWORD size;

    BITMAPINFOHEADER bih;
    BITMAPFILEHEADER bfh;

   // QFile fileObject;//文件对象
    int x,y;
    int i1,i2;
    int byte_of_74_xLine;
    char *m_pbmpdata_74;
    char *pkl_74;
    BITMAPINFO Bitinfo_74;
    int x_tu_74,y_tu_74;
    int len_tu_74;

    int xLine_add_74;

    ///////////
    QFile file_74_moban(small_file);

    QString str_disp;
    int  find_point_sum;
    int  Positon_now_find_bad;
    QString filename;
    QFile bf;
    QFile file_90_moban(source_file);

    find_point_sum=0;
    Positon_now_find_bad=0;


    file_90_moban.open(QIODevice::ReadOnly);
    len_tu_90=file_90_moban.size();
  //  file_90_moban.close();

    QDataStream  dataStreamObject90(&file_90_moban);
   // file_90_moban.Open(source_file,CFile::modeReadWrite);
   // len_tu_90=file_90_moban.GetLength();
    file_90_moban.seek(14);
    m_pbmpdata_90=new  char[len_tu_90-14];
    pkl_90=new char [len_tu_90];

    dataStreamObject90.readRawData(m_pbmpdata_90,len_tu_90-14);

    file_90_moban.seek(0);

    dataStreamObject90.readRawData(pkl_90,len_tu_90);
    file_90_moban.close();

    memset(&Bitinfo_90,0,sizeof(BITMAPINFO));
    memcpy(&Bitinfo_90,m_pbmpdata_90,sizeof(BITMAPINFO));
    x_tu_90=Bitinfo_90.bmiHeader.biWidth;
    y_tu_90=Bitinfo_90.bmiHeader.biHeight;

    if(Bitinfo_90.bmiHeader.biBitCount==32)
    {
      byte_of_90_xLine=4*x_tu_90;//32bit bmp每行字节数是4*宽度，是4的倍数
      xLine_add_90=byte_of_90_xLine- int(byte_of_90_xLine/4)*4;
      size=x_tu_90*4*y_tu_90;
    }else    if(Bitinfo_90.bmiHeader.biBitCount==24)
    {
        byte_of_90_xLine=3*x_tu_90;
        xLine_add_90=byte_of_90_xLine- int(byte_of_90_xLine/4)*4;
        if(xLine_add_90!=0)
        {
            xLine_add_90=4-xLine_add_90;
        }else
        {
                xLine_add_90=0;
        }
        byte_of_90_xLine=byte_of_90_xLine+xLine_add_90;
        size=byte_of_90_xLine*y_tu_90;
    }



    if(0)
    {
       // AfxMessageBox("not find -->  "+small_file);
        exit(0);
    }
    else
    {
    file_74_moban.open(QIODevice::ReadOnly);
    len_tu_74=file_74_moban.size();
    file_74_moban.seek(14);
    m_pbmpdata_74=new  char[len_tu_74-14];
    pkl_74=new char [len_tu_74];
    QDataStream  dataStreamObject74(&file_74_moban);
    dataStreamObject74.readRawData(m_pbmpdata_74,len_tu_74-14);

    file_74_moban.seek(0);

    dataStreamObject74.readRawData(pkl_74,len_tu_74);
    file_74_moban.close();
    }

    memset(&Bitinfo_74,0,sizeof(BITMAPINFO));
    memcpy(&Bitinfo_74,m_pbmpdata_74,sizeof(BITMAPINFO));
    x_tu_74=Bitinfo_74.bmiHeader.biWidth;
    y_tu_74=Bitinfo_74.bmiHeader.biHeight;


    if(Bitinfo_74.bmiHeader.biBitCount==32)
    {
        byte_of_74_xLine=4*x_tu_74;//32bit bmp每行字节数是4*宽度，是4的倍数
        xLine_add_74=byte_of_74_xLine- int(byte_of_74_xLine/4)*4;

        //size=x_tu_90*4*y_tu_90;
    }else    if(Bitinfo_74.bmiHeader.biBitCount==24)
    {
        byte_of_74_xLine=3*x_tu_74;//24bit bmp每行字节数是3*宽度+余数，是4的倍数
        xLine_add_74=byte_of_74_xLine- int(byte_of_74_xLine/4)*4;
        if(xLine_add_74!=0)
        {
            xLine_add_74=4-xLine_add_74;
        }else
        {
            xLine_add_74=0;
        }
        byte_of_74_xLine=byte_of_74_xLine+xLine_add_74;
    }


    //////////
    x=insert_x;y=insert_y;


    if(Bitinfo_74.bmiHeader.biBitCount==32)
    {
       bih.biBitCount=32;
    }else if(Bitinfo_74.bmiHeader.biBitCount==24)
    {
       bih.biBitCount=24;
    }
    bih.biClrImportant=0;
    bih.biClrUsed=0;
    bih.biCompression=0;

    bih.biHeight=y_tu_90;
    bih.biPlanes=1;
    bih.biSize=sizeof(BITMAPINFOHEADER);
    bih.biSizeImage=size;

    bih.biWidth=x_tu_90;
    bih.biXPelsPerMeter=0;
    bih.biYPelsPerMeter=0;



    filename=big_file;


    bfh.bfReserved1=bfh.bfReserved2=0;
    bfh.bfType=((WORD)('M'<<8|'B'));
    bfh.bfSize=54+size;
    bfh.bfOffBits=54;

    if(Bitinfo_74.bmiHeader.biBitCount==32 && Bitinfo_90.bmiHeader.biBitCount==32)
    {
      for(i1=0;i1<x_tu_74;i1++) //
      {
        for(i2=0;i2<y_tu_74;i2++)  //
        {
            if((pkl_90[54+(i2+y)*byte_of_90_xLine+4*(x+i1)+0]!=0)&&(pkl_90[54+(i2+y)*byte_of_90_xLine+4*(x+i1)+1]!=0))
            {   //只移动不是白点的点
                pkl_90[54+(i2+y)*byte_of_90_xLine+4*(x+i1)+0]=pkl_74[54+i2*byte_of_74_xLine+4*i1+0];
                pkl_90[54+(i2+y)*byte_of_90_xLine+4*(x+i1)+1]=pkl_74[54+i2*byte_of_74_xLine+4*i1+1];
                pkl_90[54+(i2+y)*byte_of_90_xLine+4*(x+i1)+2]=pkl_74[54+i2*byte_of_74_xLine+4*i1+2];
                pkl_90[54+(i2+y)*byte_of_90_xLine+4*(x+i1)+3]=pkl_74[54+i2*byte_of_74_xLine+4*i1+3];
            }

        }
      }
    }else if(Bitinfo_74.bmiHeader.biBitCount==24 && Bitinfo_90.bmiHeader.biBitCount==24)
    {
        for(i1=0;i1<x_tu_74;i1++) //
        {
            for(i2=0;i2<y_tu_74;i2++)  //
            {
                if((pkl_90[54+(i2+y)*byte_of_90_xLine+3*(x+i1)+0]!=0)&&(pkl_90[54+(i2+y)*byte_of_90_xLine+3*(x+i1)+1]!=0))
                {  //只移动不是白点的点
                   pkl_90[54+(i2+y)*byte_of_90_xLine+3*(x+i1)+0]=pkl_74[54+i2*byte_of_74_xLine+3*i1+0];
                   pkl_90[54+(i2+y)*byte_of_90_xLine+3*(x+i1)+1]=pkl_74[54+i2*byte_of_74_xLine+3*i1+1];
                   pkl_90[54+(i2+y)*byte_of_90_xLine+3*(x+i1)+2]=pkl_74[54+i2*byte_of_74_xLine+3*i1+2];
                }


            }
        }
    }



    bf.setFileName(filename);
    if(bf.open(QIODevice::WriteOnly))
    {
        QDataStream  dataStreamObject(&bf);
        dataStreamObject.writeRawData(pkl_90,54+size);
        bf.close();
    }
    delete  m_pbmpdata_74;
    delete  pkl_74;
    delete  m_pbmpdata_90;
    delete  pkl_90;

}

void MainWindow::on_pushButton_clicked()
{
   // Insert_small_bmp_to_big_bmp( 0,  0, "big_file.bmp", "安.bmp","source.bmp");
   // Insert_small_bmp_to_big_bmp( 100,  100, "big_file.bmp", "安.bmp","big_file.bmp");
    QString  txt  = ui->textEdit->toPlainText();
    writeBMP(txt);
}



void MainWindow::writeBMP(QString noteStr)
{
    // TODO: Add your control notification handler code here
    //实际图片大小：宽500*高800 像素  //测试时每个汉字60*60，字母30*60
    //每个汉字大小：宽30*高35，每页纸20行，无行距
    //每个字母：宽15*高35
    QString str;
    QString str_reconginte;
    QString str_temp;
    QString str_for_save;
    QString  word_ku[2000]; //实用字已经超过1000个，所以改为2000，应该为实际数，这样循环的效率要高一些，速度能快些
    QString  word_translate[2000];
    QString insert_file;
    QString creat_file;
    QString file_name_last;


    int word_width,word_height_no_fix;  //单个英文字母的宽度，高度，像素，汉字宽是字母的2倍
    int word_height,word_width_no_fix;
    int now_insert_pos_x,now_insert_pos_y;//目前图片融合在大图内的坐标
    int str_cmp;
    int str_len;
    int rec_len;
    int i,i1,i2,i3,i4,i5;  //i文本框内字符总长度
    int find_sigle; //单字节字符识别标志  1相同  0不同
    int find_double =1;//双字节汉字等识别标志 1相同  0不同
    int cmp_enter;//回车换行检测标志，0相同，其余不同
    int big_picture_width;//合成以后的图片宽度
    int big_picture_height;//合成以后的图片高度
    int big_picture_numeric;//和成大图的数量
    int max_line_in_big_picture; //每张图允许的最大行数
    int every_line_int[50];//每个控件返回的int值
    int infact_pos_y[50];//实际每行应该写入的y坐标值

    every_line_int[0]=0;

//    GetDlgItem(IDC_EDIT4)->GetWindowText(str);
//    every_line_int[1]=atoi(str);

    for(int m2=0;m2<50;m2++)
    {
        every_line_int[m2] =5;
    }


    word_height=70;word_width=32;//以后需要修改，临时测试用的
    word_height_no_fix=70;word_width_no_fix=40;
    now_insert_pos_x=0;now_insert_pos_y=0;
    //big_picture_width=600;
    big_picture_width=1000;
    //big_picture_height=170;
    big_picture_height=1600;
    max_line_in_big_picture=20;  // (800/60=13)
    big_picture_numeric=0;

    //GetDlgItem(IDC_BUTTON9)->GetWindowText(str);
    str="多行模式";
    if(str=="多行模式")
    {
      big_picture_width=1000;
      big_picture_height=1600;
      file_name_last="source.bmp";

    }else
    {
      big_picture_width=660;
      big_picture_height=70;
       file_name_last="source2.bmp";
    }
    //GetDlgItem(IDC_EDIT2)->GetWindowText(str); //用来修正行距，使得程序更加匹配纸张，不致于出现累计误差而产生的错行
    i=5;

    word_height = i + word_height;
    max_line_in_big_picture=big_picture_height/word_height;
   // GetDlgItem(IDC_EDIT3)->GetWindowText(str); //用来修正行距，使得程序更加匹配纸张，不致于出现累计误差而产生的错行
    i= 0 ;
    word_width=i+word_width;

    for(i1=0;i1<50;i1++)
    {
        i4=0;
        for(i3=0;i3<=i1;i3++)
        {
            i4=every_line_int[i3]+i4;
        }
        infact_pos_y[i1]=big_picture_height-(word_height_no_fix*i1+i4)-word_height_no_fix  ;
    }


    creat_file = QString::number(big_picture_numeric);
    creat_file="big_old_"+creat_file+".bmp";
    word_ku[0]="0";word_ku[1]="1";word_ku[2]="2";word_ku[3]="3";word_ku[4]="4";

    word_ku[5]="5";word_ku[6]="6";word_ku[7]="7";word_ku[8]="8";word_ku[9]="9";
    word_ku[10]="中";word_ku[11]="华";word_ku[12]="人";word_ku[13]="民";word_ku[14]="共";
    word_ku[15]="和";word_ku[16]="国";word_ku[17]=",";word_ku[18]="."; //word_ku[9]="9";
    word_ku[19]="a";word_ku[20]="b";word_ku[21]="c";word_ku[22]="d";
    word_ku[23]="f";word_ku[24]="A";word_ku[25]="B";word_ku[26]="C";
    word_ku[27]="D";word_ku[28]="E";word_ku[29]="F";word_ku[30]="T";
    word_ku[31]="U";word_ku[32]="W";word_ku[33]="R";word_ku[34]="x";
    word_ku[35]="为";word_ku[36]="&";word_ku[37]="-";word_ku[38]=" ";
    word_ku[39]="/";word_ku[40]="任";word_ku[41]="务";word_ku[42]="宏";
    word_ku[43]="下";word_ku[44]="发";word_ku[45]="增";word_ku[46]="加";
    word_ku[47]="上";word_ku[48]="行";word_ku[49]="控";word_ku[50]="制";
    word_ku[51]="时";word_ku[52]="间";word_ku[53]="年";word_ku[54]="月";
    word_ku[55]="日";word_ku[56]="地";word_ku[57]="点";word_ku[58]="组";
    word_ku[59]="织";word_ku[60]="：";word_ku[61]="会";word_ku[62]="议";
    word_ku[63]="室";word_ku[64]="主";word_ku[65]="内";word_ku[66]="容";
    word_ku[67]="渭";word_ku[68]="校";word_ku[69]="赵";word_ku[70]="伟";
    word_ku[71]="潘";word_ku[72]="仲";word_ku[73]="《";word_ku[74]="》";
    word_ku[75]="党";word_ku[76]="的";word_ku[77]="十";word_ku[78]="九";
    word_ku[79]="八";word_ku[80]="七";word_ku[81]="六";word_ku[82]="五";
    word_ku[83]="四";word_ku[84]="三";word_ku[85]="二";word_ku[86]="一";
    word_ku[87]="零";word_ku[88]="大";word_ku[89]="基";word_ku[90]="本";
    word_ku[91]="思";word_ku[92]="想";word_ku[93]="要";word_ku[94]="求";
    word_ku[95]="观";word_ku[96]="、";word_ku[97]="面";word_ku[98]="临";
    word_ku[99]="形";word_ku[100]="势";word_ku[101]="越";word_ku[102]="复";
    word_ku[103]="杂";word_ku[104]="，";word_ku[105]="肩";word_ku[106]="负";
    word_ku[107]="艰";word_ku[108]="巨";word_ku[109]="。";word_ku[110]="就";
    word_ku[111]="强";word_ku[112]="纪";word_ku[113]="律";word_ku[114]="维";
    word_ku[115]="护";word_ku[116]="建";word_ku[117]="设";word_ku[118]="集";
    word_ku[119]="统";word_ku[120]="这";word_ku[121]="充";word_ku[122]="分";
    word_ku[123]="说";word_ku[124]="明";word_ku[125]="了";word_ku[126]="新";
    word_ku[127]="极";word_ku[128]="端";word_ku[129]="重";word_ku[130]="性";
    word_ku[131]="体";word_ku[132]="现";word_ku[133]="对";word_ku[134]="高";
    word_ku[135]="度";word_ku[136]="视";word_ku[137]="切";word_ku[138]="实";
    word_ku[139]="坚";word_ku[140]="定";word_ku[141]="决";word_ku[142]="心";
    word_ku[143]="是";word_ku[144]="事";word_ku[145]="业";word_ku[146]="蓬";
    word_ku[147]="勃";word_ku[148]="展";word_ku[149]="保";word_ku[150]="证";
    word_ku[151]="我";word_ku[152]="幅";word_ku[153]="员";word_ku[154]="辽";
    word_ku[155]="阔";word_ku[156]="口";word_ku[157]="众";word_ku[158]="多";
    word_ku[159]="族";word_ku[160]="家";word_ku[161]="们";word_ku[162]="靠";
    word_ku[163]="革";word_ku[164]="命";word_ku[165]="理";word_ku[166]="铁";
    word_ku[167]="起";word_ku[168]="来";word_ku[169]="马";word_ku[170]="克";
    word_ku[171]="义";word_ku[172]="政";word_ku[173]="力";word_ku[174]="量";
    word_ku[175]="所";word_ku[176]="在";word_ku[177]="经";word_ku[178]="济";
    word_ku[179]="社";word_ku[180]="团";word_ku[181]="结";word_ku[182]="进";
    word_ku[183]="步";word_ku[184]="长";word_ku[185]="治";word_ku[186]="久";
    word_ku[187]="安";word_ku[188]="根";word_ku[189]="也";word_ku[190]="全";
    word_ku[191]="各";word_ku[192]="最";word_ku[193]="利";word_ku[194]="益";
    word_ku[195]="当";word_ku[196]="前";word_ku[197]="正";word_ku[198]="处";
    word_ku[199]="于";word_ku[200]="小";word_ku[201]="康";word_ku[202]="关";
    word_ku[203]="键";word_ku[204]="期";word_ku[205]="深";word_ku[206]="化";
    word_ku[207]="改";word_ku[208]="开";word_ku[209]="放";word_ku[210]="快";
    word_ku[211]="方";word_ku[212]="式";word_ku[213]="攻";word_ku[214]="如";
    word_ku[215]="果";word_ku[216]="您";word_ku[217]="已";word_ku[218]="址";
    word_ku[219]="栏";word_ku[220]="输";word_ku[221]="入";word_ku[222]="该";
    word_ku[223]="网";word_ku[224]="页";word_ku[225]="请";word_ku[226]="确";
    word_ku[227]="其";word_ku[228]="拼";word_ku[229]="写";word_ku[230]="认";
    word_ku[231]="检";word_ku[232]="查";word_ku[233]="络";word_ku[234]="连";
    word_ku[235]="接";word_ku[236]="单";word_ku[237]="击";word_ku[238]="具";
    word_ku[239]="工";word_ku[240]="菜";word_ku[241]="然";word_ku[242]="后";
    word_ku[243]="I";word_ku[244]="n";word_ku[245]="t";word_ku[246]="e";
    word_ku[247]="r";word_ku[248]="选";word_ku[249]="项";word_ku[250]="置";
    word_ku[251]="须";word_ku[252]="必";word_ku[253]="与";word_ku[254]="局";
    word_ku[255]="域";word_ku[256]="L";word_ku[257]="(";word_ku[258]=")";
    word_ku[259]="N";word_ku[260]="卡";word_ku[261]="管";word_ku[262]="或";
    word_ku[263]="服";word_ku[264]="供";word_ku[265]="应";word_ku[266]="商";
    word_ku[267]="S";word_ku[268]="P";word_ku[269]="提";word_ku[270]="致";
    word_ku[271]="看";word_ku[272]="否";word_ku[273]="被";word_ku[274]="测";
    word_ku[275]="可";word_ku[276]="能";word_ku[277]="让";word_ku[278]="M";
    word_ku[279]="i";word_ku[280]="o";word_ku[281]="s";word_ku[282]="w";
    word_ku[283]="站";word_ku[284]="并";word_ku[285]="自";word_ku[286]="动";
    word_ku[287]="启";word_ku[288]="用";word_ku[289]="此";word_ku[290]="他";
    word_ku[291]="错";word_ku[292]="误";word_ku[293]="刷";word_ku[294]="按";
    word_ku[295]="钮";word_ku[296]=":";word_ku[297]="稍";word_ku[298]="试";
    word_ku[299]="择";word_ku[300]="某";word_ku[301]="些";word_ku[302]="位";
    word_ku[303]="帮";word_ku[304]="助";word_ku[305]="以";word_ku[306]="装";
    word_ku[307]="访";word_ku[308]="问";word_ku[309]="支";word_ku[310]="持";
    word_ku[311]="够";word_ku[312]="级";word_ku[313]="滚";word_ku[314]="“";
    word_ku[315]="”";word_ku[316]=">";word_ku[317]="<";word_ku[318]="到";
    word_ku[319]="部";word_ku[320]="尝";word_ku[321]="链";word_ku[322]="找";
    word_ku[323]="不";word_ku[324]="器";word_ku[325]="p";word_ku[326]="l";
    word_ku[327]="无";word_ku[328]="法";word_ku[329]="显";word_ku[330]="示";
    word_ku[331]="遇";word_ku[332]="题";word_ku[333]="者";word_ku[334]="需";
    word_ku[335]="调";word_ku[336]="整";word_ku[337]="浏";word_ku[338]="览";
    word_ku[339]="图";word_ku[340]="修";word_ku[341]="诊";word_ku[342]="断";
    word_ku[343]="表";word_ku[344]="扫";word_ku[345]="回";word_ku[346]="距";
    word_ku[347]="始";word_ku[348]="束";word_ku[349]="离";word_ku[350]="相";
    word_ku[351]="普";word_ku[352]="勒";word_ku[353]="补";word_ku[354]="偿";
    word_ku[355]="作";word_ku[356]="模";word_ku[357]="代";word_ku[358]="号";
    word_ku[359]="备";word_ku[360]="合";word_ku[361]="轨";word_ku[362]="道";
    word_ku[363]="数";word_ku[364]="据";word_ku[365]="送";word_ku[366]="目";
    word_ku[367]="标";word_ku[368]="配";word_ku[369]="系";word_ku[370]="环";
    word_ku[371]="路";word_ku[372]="速";word_ku[373]="令";word_ku[374]="信";
    word_ku[375]="有";word_ku[376]="效";word_ku[377]="注";word_ku[378]="H";
    word_ku[379]="O";word_ku[380]="{";word_ku[381]="}";word_ku[382]=";";
    word_ku[383]="名";word_ku[384]="称";word_ku[385]="类";word_ku[386]="型";
    word_ku[387]="欢";word_ku[388]="迎";word_ku[389]="使";word_ku[390]="种";
    word_ku[391]="获";word_ku[392]="得";word_ku[393]="程";word_ku[394]="协";
    word_ku[395]="远";word_ku[396]="朋";word_ku[397]="友";word_ku[398]="从";
    word_ku[399]="何";word_ku[400]="计";word_ku[401]="算";word_ku[402]="机";
    word_ku[403]="联";word_ku[404]="线";word_ku[405]="专";word_ku[406]="答";
    word_ku[407]="闻";word_ku[408]="户";word_ku[409]="通";word_ku[410]="论";
    word_ku[411]="坛";word_ku[412]="过";word_ku[413]="例";word_ku[414]="电";
    word_ku[415]="子";word_ku[416]="邮";word_ku[417]="件";word_ku[418]="话";
    word_ku[419]="幕";word_ku[420]="屏";word_ku[421]="还";word_ku[422]="甚";
    word_ku[423]="至";word_ku[424]="允";word_ku[425]="许";word_ku[426]="技";
    word_ku[427]="术";word_ku[428]="解";word_ku[429]="庭";word_ku[430]="成";
    word_ku[431]="同";word_ku[432]="样";word_ku[433]="操";word_ku[434]="鑫";
    word_ku[435]="蓝";word_ku[436]="版";word_ku[437]="仅";word_ku[438]="研";
    word_ku[439]="究";word_ku[440]="盘";word_ku[441]="勿";word_ku[442]="构";
    word_ku[443]="将";word_ku[444]="非";word_ku[445]="途";word_ku[446]="完";
    word_ku[447]="因";word_ku[448]="光";word_ku[449]="由";word_ku[450]="带";
    word_ku[451]="责";word_ku[452]="兼";word_ku[453]="硬";word_ku[454]="软";
    word_ku[455]="个";word_ku[456]="息";word_ku[457]="哪";word_ku[458]="序";
    word_ku[459]="好";word_ku[460]="买";word_ku[461]="之";word_ku[462]="预";
    word_ku[463]="它";word_ku[464]="状";word_ku[465]="态";word_ku[466]="直";
    word_ku[467]="运";word_ku[468]="稳";word_ku[469]="平";word_ku[470]="怎";
    word_ku[471]="知";word_ku[472]="造";word_ku[473]="产";word_ku[474]="品";
    word_ku[475]="搜";word_ku[476]="索";word_ku[477]="及";word_ku[478]="更";
    word_ku[479]="很";word_ku[480]="情";word_ku[481]="况";word_ku[482]="独";
    word_ku[483]="立";word_ku[484]="评";word_ku[485]="际";word_ku[486]="鼓";
    word_ku[487]="励";word_ku[488]="交";word_ku[489]="码";word_ku[490]="李";
    word_ku[491]="礼";word_ku[492]="貌";word_ku[493]="你";word_ku[494]="吃";
    word_ku[495]="去";word_ku[496]="西";word_ku[497]="咸";word_ku[498]="阳";
    word_ku[499]="泉";word_ku[500]="亭";word_ku[501]="玉";word_ku[502]="季";
    word_ku[503]="桃";word_ku[504]="花";word_ku[505]="云";word_ku[506]="流";
    word_ku[507]="水";word_ku[508]="南";word_ku[509]="东";word_ku[510]="北";
    word_ku[511]="喝";word_ku[512]="河";word_ku[513]="王";word_ku[514]="张";
    word_ku[515]="邢";word_ku[516]="苹";word_ku[517]="梨";word_ku[518]="香";
    word_ku[519]="蕉";word_ku[520]="红";word_ku[521]="柿";word_ku[522]="辣";
    word_ku[523]="椒";word_ku[524]="瓜";word_ku[525]="葡";word_ku[526]="萄";
    word_ku[527]="草";word_ku[528]="莓";word_ku[529]="黑";word_ku[530]="赤";
    word_ku[531]="橙";word_ku[532]="绿";word_ku[533]="紫";word_ku[534]="色";
    word_ku[535]="牛";word_ku[536]="老";word_ku[537]="虎";word_ku[538]="兔";
    word_ku[539]="停";word_ku[540]="走";word_ku[541]="猪";word_ku[542]="头";
    word_ku[543]="学";word_ku[544]="医";word_ku[545]="院";word_ku[546]="狗";
    word_ku[547]="咪";word_ku[548]="猫";word_ku[549]="太";word_ku[550]="亮";
    word_ku[551]="桶";word_ku[552]="镜";word_ku[553]="美";word_ku[554]="丽";
    word_ku[555]="漂";word_ku[556]="丑";word_ku[557]="爱";word_ku[558]="胖";
    word_ku[559]="矮";word_ku[560]="瘦";word_ku[561]="良";word_ku[562]="秀";
    word_ku[563]="优";word_ku[564]="游";word_ku[565]="戏";word_ku[566]="银";
    word_ku[567]="超";word_ku[568]="市";word_ku[569]="车";word_ku[570]="场";
    word_ku[571]="广";word_ku[572]="球";word_ku[573]="灯";word_ku[574]="史";
    word_ku[575]="历";word_ku[576]="略";word_ku[577]="志";word_ku[578]="报";
    word_ku[579]="告";word_ku[580]="述";word_ku[581]="职";word_ku[582]="（";
    word_ku[583]="）";word_ku[584]="尊";word_ku[585]="敬";word_ku[586]="委";
    word_ku[587]="叫";word_ku[588]="毕";word_ku[589]="指";word_ku[590]="挥";
    word_ku[591]="航";word_ku[592]="天";word_ku[593]="空";word_ku[594]="宇";
    word_ku[595]="科";word_ku[596]="读";word_ku[597]="硕";word_ku[598]="士";
    word_ku[599]="别";word_ku[600]="份";word_ku[601]="参";word_ku[602]="勤";
    word_ku[603]="奋";word_ku[604]="踏";word_ku[605]="尽";word_ku[606]="彰";
    word_ku[607]="干";word_ku[608]="己";word_ku[609]="习";word_ku[610]="汇";
    word_ku[611]="真";word_ku[612]="贯";word_ku[613]="彻";word_ku[614]="针";
    word_ku[615]="策";word_ku[616]="终";word_ku[617]="身";word_ku[618]="素";
    word_ku[619]="养";word_ku[620]="风";word_ku[621]="军";word_ku[622]="核";
    word_ku[623]="价";word_ku[624]="值";word_ku[625]="积";word_ku[626]="念";
    word_ku[627]="忠";word_ku[628]="诚";word_ku[629]="履";word_ku[630]="等";
    word_ku[631]="教";word_ku[632]="育";word_ku[633]="记";word_ku[634]="录";
    word_ku[635]="笔";word_ku[636]="活";word_ku[637]="撰";word_ku[638]="    ";
    word_ku[639]="验";word_ku[640]="岗";word_ku[641]="先";word_ku[642]="诺";
    word_ku[643]="神";word_ku[644]="哨";word_ku[645]="尖";word_ku[646]="列";
    word_ku[647]="兵";word_ku[648]="斗";word_ku[649]="次";word_ku[650]="担";
    word_ku[651]="总";word_ku[652]="每";word_ku[653]="都";word_ku[654]="做";
    word_ku[655]="圆";word_ku[656]="满";word_ku[657]="常";word_ku[658]="仔";
    word_ku[659]="细";word_ku[660]="班";word_ku[661]="累";word_ku[662]="近";
    word_ku[663]="圈";word_ku[664]="识";word_ku[665]="懂";word_ku[666]="虚";
    word_ku[667]="向";word_ku[668]="周";word_ku[669]="围";word_ku[670]="紧";
    word_ku[671]="贴";word_ku[672]="础";word_ku[673]="促";word_ku[674]="较";
    word_ku[675]="短";word_ku[676]="掌";word_ku[677]="握";word_ku[678]="原";
    word_ku[679]="规";word_ku[680]="契";word_ku[681]="厂";word_ku[682]="里";
    word_ku[683]="收";word_ku[684]="资";word_ku[685]="料";word_ku[686]="万";
    word_ku[687]="字";word_ku[688]="践";word_ku[689]="出";word_ku[690]="打";
    word_ku[691]="故";word_ku[692]="障";word_ku[693]="落";word_ku[694]="划";
    word_ku[695]="隐";word_ku[696]="患";word_ku[697]="排";word_ku[698]="限";
    word_ku[699]="降";word_ku[700]="低";word_ku[701]="影";word_ku[702]="响";
    word_ku[703]="隙";word_ku[704]="休";word_ku[705]="反";word_ku[706]="功";
    word_ku[707]="傅";word_ku[708]="师";word_ku[709]="第";word_ku[710]="手";
    word_ku[711]="跳";word_ku[712]="双";word_ku[713]="捕";word_ku[714]="异";
    word_ku[715]="换";word_ku[716]="余";word_ku[717]="缺";word_ku[718]="失";
    word_ku[719]="返";word_ku[720]="翻";word_ku[721]="纸";word_ku[722]="板";
    word_ku[723]="转";word_ku[724]="文";word_ku[725]="片";word_ku[726]="芯";
    word_ku[727]="案";word_ku[728]="冗";word_ku[729]="脚";word_ku[730]="替";
    word_ku[731]="顺";word_ku[732]="执";word_ku[733]="气";word_ku[734]="折";
    word_ku[735]="射";word_ku[736]="段";word_ku[737]="待";word_ku[738]="析";
    word_ku[739]="届";word_ku[740]="奖";word_ku[741]="领";word_ku[742]="导";
    word_ku[743]="率";word_ku[744]="元";word_ku[745]="锁";word_ku[746]="取";
    word_ku[747]="绩";word_ku[748]="但";word_ku[749]="差";word_ku[750]="努";
    word_ku[751]="夯";word_ku[752]="拓";word_ku[753]="姿";word_ku[754]="饱";
    word_ku[755]="投";word_ku[756]="热";word_ku[757]="钻";word_ku[758]="精";
    word_ku[759]="创";word_ku[760]="突";word_ku[761]="破";word_ku[762]="争";
    word_ku[763]="几";word_ku[764]="篇";word_ku[765]="简";word_ku[766]="质";
    word_ku[767]="批";word_ku[768]="繁";word_ku[769]="忙";word_ku[770]="觉";
    word_ku[771]="即";word_ku[772]="滴";word_ku[773]="感";word_ku[774]="生";
    word_ku[775]="足";word_ku[776]="言";word_ku[777]="忘";word_ku[778]="只";
    word_ku[779]="训";word_ku[780]="存";word_ku[781]="源";word_ku[782]="径";
    word_ku[783]="才";word_ku[784]="未";word_ku[785]="今";word_ku[786]="布";
    word_ku[787]="密";word_ku[788]="条";word_ku[789]="听";word_ku[790]="讲";
    word_ku[791]="阅";word_ku[792]="特";word_ku[793]="层";word_ku[794]="雷";
    word_ku[795]="锋";word_ku[796]="郭";word_ku[797]="迹";word_ku[798]="触";
    word_ku[799]="颇";word_ku[800]="那";word_ku[801]="兢";word_ku[802]="献";
    word_ku[803]="血";word_ku[804]="贫";word_ku[805]="困";word_ku[806]="私";
    word_ku[807]="清";word_ku[808]="楚";word_ku[809]="古";word_ku[810]="彼";
    word_ku[811]="百";word_ku[812]="战";word_ku[813]="殆";word_ku[814]="飞";
    word_ku[815]="谁";word_ku[816]="秘";word_ku[817]="刃";word_ku[818]="权";
    word_ku[819]="而";word_ku[820]="胜";word_ku[821]="尤";word_ku[822]="刻";
    word_ku[823]="醒";word_ku[824]="警";word_ku[825]="钟";word_ku[826]="鸣";
    word_ku[827]="防";word_ku[828]="燃";word_ku[829]="练";word_ku[830]="且";
    word_ku[831]="没";word_ku[832]="少";word_ku[833]="给";word_ku[834]="遵";
    word_ku[835]="守";word_ku[836]="严";word_ku[837]="章";word_ku[838]="考";/**/
    word_ku[839]="熟";word_ku[840]="适";word_ku[841]="牢";word_ku[842]="悉";
    word_ku[843]="格";word_ku[844]="敢";word_ku[845]="旁";word_ku[846]="边";
    word_ku[847]="达";word_ku[848]="耳";word_ku[849]="颗";word_ku[850]="疾";
    word_ku[851]="病";word_ku[852]="痛";word_ku[853]="苦";word_ku[854]="编";
    word_ku[855]="演";word_ku[856]="急";word_ku[857]="书";word_ku[858]="爬";
    word_ku[859]="借";word_ku[860]="晚";word_ku[861]="末";word_ku[862]="承";
    word_ku[863]="初";word_ku[864]="智";word_ku[865]="－";word_ku[866]="销";
    word_ku[867]="假";word_ku[868]="依";word_ku[869]="早";word_ku[870]="意";
    word_ku[871]="象";word_ku[872]="轻";word_ku[873]="传";word_ku[874]="扩";
    word_ku[875]="频";word_ku[876]="外";word_ku[877]="；";word_ku[878]="欠";
    word_ku[879]="躁";word_ku[880]="绪";word_ku[881]="穿";word_ku[882]="卫";
    word_ku[883]="星";word_ku[884]="扰";word_ku[885]="首";word_ku[886]="！";
    word_ku[887]="半";word_ku[888]="武";word_ku[889]="脑";word_ku[890]="胡";
    word_ku[891]="难";word_ku[892]="准";word_ku[893]="顾";word_ku[894]="则";
    word_ku[895]="监";word_ku[896]="骨";word_ku[897]="队";word_ku[898]="杨";
    word_ku[899]="海";word_ku[900]="龙";word_ku[901]="男";word_ku[902]="汉";
    word_ku[903]="陕";word_ku[904]="乾";word_ku[905]="县";word_ku[906]="吉";
    word_ku[907]="林";word_ku[908]="嘉";word_ku[909]="拥";word_ku[910]="央";
    word_ku[911]="照";word_ku[912]="宗";word_ku[913]="旨";word_ku[914]="培";
    word_ku[915]="尚";word_ku[916]="德";word_ku[917]="健";word_ku[918]="趣";
    word_ku[919]="门";word_ku[920]="劳";word_ku[921]="怨";word_ku[922]="凡";
    word_ku[923]="台";word_ku[924]="烧";word_ku[925]="毁";word_ku[926]="晶";
    word_ku[927]="振";word_ku[928]="帧";word_ku[929]="遥";word_ku[930]="比";
    word_ku[931]="烽";word_ku[932]="火";word_ku[933]="施";word_ku[934]="希";
    word_ku[935]="望";word_ku[936]="耗";word_ku[937]="巡";word_ku[938]="公";
    word_ku[939]="措";word_ku[940]="畏";word_ku[941]="虽";word_ku[942]="偏";
    word_ku[943]="薄";word_ku[944]="弱";word_ku[945]="唐";word_ku[946]="倾";
    word_ku[947]="籍";word_ku[948]="善";word_ku[949]="白";word_ku[950]="刊";
    word_ku[951]="综";word_ku[952]="吴";word_ku[953]="扬";word_ku[954]="乏";
    word_ku[955]="材";word_ku[956]="搞";word_ku[957]="刚";word_ku[958]="候";
    word_ku[959]="恰";word_ku[960]="逢";word_ku[961]="怕";word_ku[962]="逐";
    word_ku[963]="渐";word_ku[964]="宝";word_ku[965]="贵";word_ku[966]="底";
    word_ku[967]="督";word_ku[968]="妥";word_ku[969]="移";word_ku[970]="载";
    word_ku[971]="登";word_ku[972]="谣";word_ku[973]="惕";word_ku[974]="松";
    word_ku[975]="互";word_ku[976]="娱";word_ku[977]="乐";word_ku[978]="两";
    word_ku[979]="角";word_ku[980]="摘";word_ku[981]="【";word_ku[982]="】";
    word_ku[983]="沿";word_ku[984]="椭";word_ku[985]="引";word_ku[986]="迭";
    word_ku[987]="讨";word_ku[988]="推";word_ku[989]="采";word_ku[990]="顿";
    word_ku[991]="拟";word_ku[992]="典";word_ku[993]="敛";word_ku[994]="阶";
    word_ku[995]="慢";word_ku[996]="消";word_ku[997]="宙";word_ku[998]="渔";
    word_ku[999]="舟";word_ku[1000]="石";word_ku[1001]="雨";word_ku[1002]="豆";
    word_ku[1003]="浆";word_ku[1004]="阻";word_ku[1005]="塞";word_ku[1006]="另";
    word_ku[1007]="疑";word_ku[1008]="描";word_ku[1009]="轮";word_ku[1010]="厘";
    word_ku[1011]="吏";word_ku[1012]="哩";word_ku[1013]="犁";word_ku[1014]="黎";
    word_ku[1015]="篱";word_ku[1016]="厉";word_ku[1017]="莉";word_ku[1018]="砾";
    word_ku[1019]="鹂";word_ku[1020]="呵";word_ku[1021]="哈";word_ku[1022]="柱";
    word_ku[1023]="席";word_ku[1024]="梦";word_ku[1025]="富";word_ku[1026]="英";
    word_ku[1027]="哲";word_ku[1028]="曾";word_ku[1029]="诗";word_ku[1030]="灵";
    word_ku[1031]="透";word_ku[1032]="物";word_ku[1033]="沉";word_ku[1034]="伦";
    word_ku[1035]="庄";word_ku[1036]="逻";word_ku[1037]="辑";word_ku[1038]="辞";
    word_ku[1039]="辩";word_ku[1040]="\"";word_ku[1041]="句";word_ku[1042]="鉴";
    word_ku[1043]="鲜";word_ku[1044]="绎";word_ku[1045]="块";word_ku[1046]="再";
    word_ku[1047]="咱";word_ku[1048]="辛";word_ku[1049]="铭";word_ku[1050]="介";
    word_ku[1051]="绍";word_ku[1052]="托";word_ku[1053]="着";word_ku[1054]="雄";
    word_ku[1055]="树";word_ku[1056]="榜";word_ku[1057]="官";word_ku[1058]="窗";
    word_ku[1059]="荣";word_ku[1060]="艺";word_ku[1061]="…";word_ku[1062]="避";
    word_ku[1063]="蕴";word_ku[1064]="含";word_ku[1065]="觑";word_ku[1066]="眼";
    word_ku[1067]="晓";word_ku[1068]="歌";word_ku[1069]="懈";word_ku[1070]="易";
    word_ku[1071]="矣";word_ku[1072]="亿";word_ku[1073]="毅";word_ku[1074]="译";
    word_ku[1075]="闷";word_ku[1076]="扪";word_ku[1077]="冠";word_ku[1078]="惯";
    word_ku[1079]="灌";word_ku[1080]="罐";word_ku[1081]="馆";word_ku[1082]="纳";
    word_ku[1083]="棺";word_ku[1084]="仑";word_ku[1085]="纶";word_ku[1086]="囵";
    word_ku[1087]="沦";word_ku[1088]="柯";word_ku[1089]="棵";word_ku[1090]="客";
    word_ku[1091]="课";word_ku[1092]="壳";word_ku[1093]="园";word_ku[1094]="春";
    word_ku[1095]="湖";word_ku[1096]="硫";word_ku[1097]="黄";word_ku[1098]="皇";
    word_ku[1099]="刘";word_ku[1100]="邓";word_ku[1101]="恩";word_ku[1102]="京";
    word_ku[1103]="幻";word_ku[1104]="稀";word_ku[1105]="毛";word_ku[1106]="泽";
    word_ku[1107]="妈";word_ku[1108]="爸";word_ku[1109]="祖";word_ku[1110]="岁";
    word_ku[1111]="宁";word_ku[1112]="斯";word_ku[1113]="剩";word_ku[1114]="纲";
    word_ku[1115]="润";word_ku[1116]="筑";word_ku[1117]="尔";word_ku[1118]="儿";
    word_ku[1119]="贰";word_ku[1120]="饵";word_ku[1121]="诱";word_ku[1122]="腐";
    word_ku[1123]="倡";word_ku[1124]="廉";word_ku[1125]="墙";word_ku[1126]="抢";
    word_ku[1127]="枪";word_ku[1128]="腔";word_ku[1129]="羌";word_ku[1130]="蔷";
    word_ku[1131]="铿";word_ku[1132]="锵";word_ku[1133]="玫";word_ku[1134]="瑰";
    word_ku[1135]="孙";word_ku[1136]="山";word_ku[1137]="爷";word_ku[1138]="奶";
    word_ku[1139]="婆";word_ku[1140]="魂";word_ku[1141]="糖";word_ku[1142]="衣";
    word_ku[1143]="炮";word_ku[1144]="弹";word_ku[1145]="贪";word_ku[1146]="污";
    word_ku[1147]="味";word_ku[1148]="福";word_ku[1149]="幸";word_ku[1150]="伍";
    word_ku[1151]="磨";word_ku[1152]="灭";word_ku[1153]="贡";word_ku[1154]="辈";
    word_ku[1155]="默";word_ku[1156]="把";word_ku[1157]="崇";word_ku[1158]="壮";
    word_ku[1159]="烈";word_ku[1160]="浓";word_ku[1161]="厚";word_ku[1162]="奉";
    word_ku[1163]="仰";word_ku[1164]="谋";word_ku[1165]="拉";word_ku[1166]="迅";
    word_ku[1167]="夺";word_ku[1168]="秒";word_ku[1169]="又";word_ku[1170]="箭";
    word_ku[1171]="挑";word_ku[1172]="世";word_ku[1173]="界";word_ku[1174]="牌";
    word_ku[1175]="举";word_ku[1176]="瞩";word_ku[1177]="激";word_ku[1178]="螺";
    word_ku[1179]="丝";word_ku[1180]="钉";word_ku[1181]="昼";word_ku[1182]="夜";
    word_ku[1183]="船";word_ku[1184]="驾";word_ku[1185]="企";word_ku[1186]="么";
    word_ku[1187]="滋";word_ku[1188]="却";word_ku[1189]="豪";word_ku[1190]="江";
    word_ku[1191]="察";word_ku[1192]="词";word_ku[1193]="劲";word_ku[1194]="辜";
    word_ku[1195]="继";word_ku[1196]="续";word_ku[1197]="妙";word_ku[1198]="辉";
    word_ku[1199]="旭";word_ku[1120]="叙";word_ku[1121]="徐";word_ku[1122]="恤";
    word_ku[1123]="嘘";word_ku[1124]="蓄";word_ku[1125]="喜";word_ku[1126]="怒";
    word_ku[1127]="哀";word_ku[1128]="洗";word_ku[1129]="吸";word_ku[1130]="兮";
    word_ku[1131]="溪";


































    word_translate[0]="0";word_translate[1]="1";word_translate[2]="2";word_translate[3]="3";word_translate[4]="4";

    word_translate[5]="5";word_translate[6]="6";word_translate[7]="7";word_translate[8]="8";word_translate[9]="9";
    word_translate[10]="中";word_translate[11]="华";word_translate[12]="人";word_translate[13]="民";word_translate[14]="共";
    word_translate[15]="和";word_translate[16]="国";word_translate[17]="单字节逗号";word_translate[18]="单字节点号"; //word_translate[9]="9";
    word_translate[19]="a";word_translate[20]="b";word_translate[21]="c";word_translate[22]="d"; //word_translate[9]="9";
    word_translate[23]="f";word_translate[24]="A_";word_translate[25]="B_";word_translate[26]="C_"; //word_translate[9]="9";
    word_translate[27]="D_";word_translate[28]="E_";word_translate[29]="F_";word_translate[30]="T_"; //word_translate[9]="9";
    word_translate[31]="U_";word_translate[32]="W_";word_translate[33]="R_";word_translate[34]="x"; //word_translate[9]="9";
    word_translate[35]="为";word_translate[36]="_and";word_translate[37]="_jianhao";word_translate[38]="_space";
    word_translate[39]="_fanxiegang";word_translate[40]="任";word_translate[41]="务";word_translate[42]="宏";
    word_translate[43]="下";word_translate[44]="发";word_translate[45]="增";word_translate[46]="加";
    word_translate[47]="上";word_translate[48]="行";word_translate[49]="控";word_translate[50]="制";
    word_translate[51]="时";word_translate[52]="间";word_translate[53]="年";word_translate[54]="月";
    word_translate[55]="日";word_translate[56]="地";word_translate[57]="点";word_translate[58]="组";
    word_translate[59]="织";word_translate[60]="双字节冒号";word_translate[61]="会";word_translate[62]="议";
    word_translate[63]="室";word_translate[64]="主";word_translate[65]="内";word_translate[66]="容";
    word_translate[67]="渭";word_translate[68]="校"; word_translate[69]="赵";word_translate[70]="伟";
    word_translate[71]="潘";word_translate[72]="仲";word_translate[73]="_书名号左";word_translate[74]="_书名号右";
    word_translate[75]="党";word_translate[76]="的";word_translate[77]="十";word_translate[78]="九";
    word_translate[79]="八";word_translate[80]="七";word_translate[81]="六";word_translate[82]="五";
    word_translate[83]="四";word_translate[84]="三";word_translate[85]="二";word_translate[86]="一";
    word_translate[87]="零";word_translate[88]="大";word_translate[89]="基";word_translate[90]="本";
    word_translate[91]="思";word_translate[92]="想";word_translate[93]="要";word_translate[94]="求";
    word_translate[95]="观";word_translate[96]="_顿号";word_translate[97]="面";word_translate[98]="临";
    word_translate[99]="形";word_translate[100]="势";word_translate[101]="越";word_translate[102]="复";
    word_translate[103]="杂";word_translate[104]="_双字节逗号";word_translate[105]="肩";word_translate[106]="负";
    word_translate[107]="艰";word_translate[108]="巨";word_translate[109]="_双字节句号";word_translate[110]="就";
    word_translate[111]="强";word_translate[112]="纪";word_translate[113]="律";word_translate[114]="维";
    word_translate[115]="护";word_translate[116]="建";word_translate[117]="设";word_translate[118]="集";
    word_translate[119]="统";word_translate[120]="这";word_translate[121]="充";word_translate[122]="分";
    word_translate[123]="说";word_translate[124]="明";word_translate[125]="了";word_translate[126]="新";
    word_translate[127]="极";word_translate[128]="端";word_translate[129]="重";word_translate[130]="性";
    word_translate[131]="体";word_translate[132]="现";word_translate[133]="对";word_translate[134]="高";
    word_translate[135]="度";word_translate[136]="视";word_translate[137]="切";word_translate[138]="实";
    word_translate[139]="坚";word_translate[140]="定";word_translate[141]="决";word_translate[142]="心";
    word_translate[143]="是";word_translate[144]="事";word_translate[145]="业";word_translate[146]="蓬";
    word_translate[147]="勃";word_translate[148]="展";word_translate[149]="保";word_translate[150]="证";
    word_translate[151]="我";word_translate[152]="幅";word_translate[153]="员";word_translate[154]="辽";
    word_translate[155]="阔";word_translate[156]="口";word_translate[157]="众";word_translate[158]="多";
    word_translate[159]="族";word_translate[160]="家";word_translate[161]="们";word_translate[162]="靠";
    word_translate[163]="革";word_translate[164]="命";word_translate[165]="理";word_translate[166]="铁";
    word_translate[167]="起";word_translate[168]="来";word_translate[169]="马";word_translate[170]="克";
    word_translate[171]="义";word_translate[172]="政";word_translate[173]="力";word_translate[174]="量";
    word_translate[175]="所";word_translate[176]="在";word_translate[177]="经";word_translate[178]="济";
    word_translate[179]="社";word_translate[180]="团";word_translate[181]="结";word_translate[182]="进";
    word_translate[183]="步";word_translate[184]="长";word_translate[185]="治";word_translate[186]="久";
    word_translate[187]="安";word_translate[188]="根";word_translate[189]="也";word_translate[190]="全";
    word_translate[191]="各";word_translate[192]="最";word_translate[193]="利";word_translate[194]="益";
    word_translate[195]="当";word_translate[196]="前";word_translate[197]="正";word_translate[198]="处";
    word_translate[199]="于";word_translate[200]="小";word_translate[201]="康";word_translate[202]="关";
    word_translate[203]="键";word_translate[204]="期";word_translate[205]="深";word_translate[206]="化";
    word_translate[207]="改";word_translate[208]="开";word_translate[209]="放";word_translate[210]="快";
    word_translate[211]="方";word_translate[212]="式";word_translate[213]="攻";word_translate[214]="如";
    word_translate[215]="果";word_translate[216]="您";word_translate[217]="已";word_translate[218]="址";
    word_translate[219]="栏";word_translate[220]="输";word_translate[221]="入";word_translate[222]="该";
    word_translate[223]="网";word_translate[224]="页";word_translate[225]="请";word_translate[226]="确";
    word_translate[227]="其";word_translate[228]="拼";word_translate[229]="写";word_translate[230]="认";
    word_translate[231]="检";word_translate[232]="查";word_translate[233]="络";word_translate[234]="连";
    word_translate[235]="接";word_translate[236]="单";word_translate[237]="击";word_translate[238]="具";
    word_translate[239]="工";word_translate[240]="菜";word_translate[241]="然";word_translate[242]="后";
    word_translate[243]="I_";word_translate[244]="n";word_translate[245]="t";word_translate[246]="e";
    word_translate[247]="r";word_translate[248]="选";word_translate[249]="项";word_translate[250]="置";
    word_translate[251]="须";word_translate[252]="必";word_translate[253]="与";word_translate[254]="局";
    word_translate[255]="域";word_translate[256]="L_";word_translate[257]="单字节左括号";word_translate[258]="单字节右括号";
    word_translate[259]="N_";word_translate[260]="卡";word_translate[261]="管";word_translate[262]="或";
    word_translate[263]="服";word_translate[264]="供";word_translate[265]="应";word_translate[266]="商";
    word_translate[267]="S_";word_translate[268]="P_";word_translate[269]="提";word_translate[270]="致";
    word_translate[271]="看";word_translate[272]="否";word_translate[273]="被";word_translate[274]="测";
    word_translate[275]="可";word_translate[276]="能";word_translate[277]="让";word_translate[278]="M_";
    word_translate[279]="i";word_translate[280]="o";word_translate[281]="s";word_translate[282]="w";
    word_translate[283]="站";word_translate[284]="并";word_translate[285]="自";word_translate[286]="动";
    word_translate[287]="启";word_translate[288]="用";word_translate[289]="此";word_translate[290]="他";
    word_translate[291]="错";word_translate[292]="误";word_translate[293]="刷";word_translate[294]="按";
    word_translate[295]="钮";word_translate[296]="单字节冒号";word_translate[297]="稍";word_translate[298]="试";
    word_translate[299]="择";word_translate[300]="某";word_translate[301]="些";word_translate[302]="位";
    word_translate[303]="帮";word_translate[304]="助";word_translate[305]="以";word_translate[306]="装";
    word_translate[307]="访";word_translate[308]="问";word_translate[309]="支";word_translate[310]="持";
    word_translate[311]="够";word_translate[312]="级";word_translate[313]="滚";word_translate[314]="双字节左双引号";
    word_translate[315]="双字节右双引号";word_translate[316]="单字节大于号";word_translate[317]="单字节小于号";word_translate[318]="到";
    word_translate[319]="部";word_translate[320]="尝";word_translate[321]="链";word_translate[322]="找";
    word_translate[323]="不";word_translate[324]="器";word_translate[325]="p";word_translate[326]="l";
    word_translate[327]="无";word_translate[328]="法";word_translate[329]="显";word_translate[330]="示";
    word_translate[331]="遇";word_translate[332]="题";word_translate[333]="者";word_translate[334]="需";
    word_translate[335]="调";word_translate[336]="整";word_translate[337]="浏";word_translate[338]="览";
    word_translate[339]="图";word_translate[340]="修";word_translate[341]="诊";word_translate[342]="断";
    word_translate[343]="表";word_translate[344]="扫";word_translate[345]="回";word_translate[346]="距";
    word_translate[347]="始";word_translate[348]="束";word_translate[349]="离";word_translate[350]="相";
    word_translate[351]="普";word_translate[352]="勒";word_translate[353]="补";word_translate[354]="偿";
    word_translate[355]="作";word_translate[356]="模";word_translate[357]="代";word_translate[358]="号";
    word_translate[359]="备";word_translate[360]="合";word_translate[361]="轨";word_translate[362]="道";
    word_translate[363]="数";word_translate[364]="据";word_translate[365]="送";word_translate[366]="目";
    word_translate[367]="标";word_translate[368]="配";word_translate[369]="系";word_translate[370]="环";
    word_translate[371]="路";word_translate[372]="速";word_translate[373]="令";word_translate[374]="信";
    word_translate[375]="有";word_translate[376]="效";word_translate[377]="注";word_translate[378]="H_";
    word_translate[379]="O_";word_translate[380]="单字节左大括号";word_translate[381]="单字节右大括号";word_translate[382]="单字节分号";
    word_translate[383]="名";word_translate[384]="称";word_translate[385]="类";word_translate[386]="型";
    word_translate[387]="欢";word_translate[388]="迎";word_translate[389]="使";word_translate[390]="种";
    word_translate[391]="获";word_translate[392]="得";word_translate[393]="程";word_translate[394]="协";
    word_translate[395]="远";word_translate[396]="朋";word_translate[397]="友";word_translate[398]="从";
    word_translate[399]="何";word_translate[400]="计";word_translate[401]="算";word_translate[402]="机";
    word_translate[403]="联";word_translate[404]="线";word_translate[405]="专";word_translate[406]="答";
    word_translate[407]="闻";word_translate[408]="户";word_translate[409]="通";word_translate[410]="论";
    word_translate[411]="坛";word_translate[412]="过";word_translate[413]="例";word_translate[414]="电";
    word_translate[415]="子";word_translate[416]="邮";word_translate[417]="件";word_translate[418]="话";
    word_translate[419]="幕";word_translate[420]="屏";word_translate[421]="还";word_translate[422]="甚";
    word_translate[423]="至";word_translate[424]="允";word_translate[425]="许";word_translate[426]="技";
    word_translate[427]="术";word_translate[428]="解";word_translate[429]="庭";word_translate[430]="成";
    word_translate[431]="同";word_translate[432]="样";word_translate[433]="操";word_translate[434]="鑫";
    word_translate[435]="蓝";word_translate[436]="版";word_translate[437]="仅";word_translate[438]="研";
    word_translate[439]="究";word_translate[440]="盘";word_translate[441]="勿";word_translate[442]="构";
    word_translate[443]="将";word_translate[444]="非";word_translate[445]="途";word_translate[446]="完";
    word_translate[447]="因";word_translate[448]="光";word_translate[449]="由";word_translate[450]="带";
    word_translate[451]="责";word_translate[452]="兼";word_translate[453]="硬";word_translate[454]="软";
    word_translate[455]="个";word_translate[456]="息";word_translate[457]="哪";word_translate[458]="序";
    word_translate[459]="好";word_translate[460]="买";word_translate[461]="之";word_translate[462]="预";
    word_translate[463]="它";word_translate[464]="状";word_translate[465]="态";word_translate[466]="直";
    word_translate[467]="运";word_translate[468]="稳";word_translate[469]="平";word_translate[470]="怎";
    word_translate[471]="知";word_translate[472]="造";word_translate[473]="产";word_translate[474]="品";
    word_translate[475]="搜";word_translate[476]="索";word_translate[477]="及";word_translate[478]="更";
    word_translate[479]="很";word_translate[480]="情";word_translate[481]="况";word_translate[482]="独";
    word_translate[483]="立";word_translate[484]="评";word_translate[485]="际";word_translate[486]="鼓";
    word_translate[487]="励";word_translate[488]="交";word_translate[489]="码";word_translate[490]="李";
    word_translate[491]="礼";word_translate[492]="貌";word_translate[493]="你";word_translate[494]="吃";
    word_translate[495]="去";word_translate[496]="西";word_translate[497]="咸";word_translate[498]="阳";
    word_translate[499]="泉";word_translate[500]="亭";word_translate[501]="玉";word_translate[502]="季";
    word_translate[503]="桃";word_translate[504]="花";word_translate[505]="云";word_translate[506]="流";
    word_translate[507]="水";word_translate[508]="南";word_translate[509]="东";word_translate[510]="北";
    word_translate[511]="喝";word_translate[512]="河";word_translate[513]="王";word_translate[514]="张";
    word_translate[515]="邢";word_translate[516]="苹";word_translate[517]="梨";word_translate[518]="香";
    word_translate[519]="蕉";word_translate[520]="红";word_translate[521]="柿";word_translate[522]="辣";
    word_translate[523]="椒";word_translate[524]="瓜";word_translate[525]="葡";word_translate[526]="萄";
    word_translate[527]="草";word_translate[528]="莓";word_translate[529]="黑";word_translate[530]="赤";
    word_translate[531]="橙";word_translate[532]="绿";word_translate[533]="紫";word_translate[534]="色";
    word_translate[535]="牛";word_translate[536]="老";word_translate[537]="虎";word_translate[538]="兔";
    word_translate[539]="停";word_translate[540]="走";word_translate[541]="猪";word_translate[542]="头";
    word_translate[543]="学";word_translate[544]="医";word_translate[545]="院";word_translate[546]="狗";
    word_translate[547]="咪";word_translate[548]="猫";word_translate[549]="太";word_translate[550]="亮";
    word_translate[551]="桶";word_translate[552]="镜";word_translate[553]="美";word_translate[554]="丽";
    word_translate[555]="漂";word_translate[556]="丑";word_translate[557]="爱";word_translate[558]="胖";
    word_translate[559]="矮";word_translate[560]="瘦";word_translate[561]="良";word_translate[562]="秀";
    word_translate[563]="优";word_translate[564]="游";word_translate[565]="戏";word_translate[566]="银";
    word_translate[567]="超";word_translate[568]="市";word_translate[569]="车";word_translate[570]="场";
    word_translate[571]="广";word_translate[572]="球";word_translate[573]="灯";word_translate[574]="史";
    word_translate[575]="历";word_translate[576]="略";word_translate[577]="志";word_translate[578]="报";
    word_translate[579]="告";word_translate[580]="述";word_translate[581]="职";word_translate[582]="_双字节左括号";
    word_translate[583]="_双字节右括号";word_translate[584]="尊";word_translate[585]="敬";word_translate[586]="委";
    word_translate[587]="叫";word_translate[588]="毕";word_translate[589]="指";word_translate[590]="挥";
    word_translate[591]="航";word_translate[592]="天";word_translate[593]="空";word_translate[594]="宇";
    word_translate[595]="科";word_translate[596]="读";word_translate[597]="硕";word_translate[598]="士";
    word_translate[599]="别";word_translate[600]="份";word_translate[601]="参";word_translate[602]="勤";
    word_translate[603]="奋";word_translate[604]="踏";word_translate[605]="尽";word_translate[606]="彰";
    word_translate[607]="干";word_translate[608]="己";word_translate[609]="习";word_translate[610]="汇";
    word_translate[611]="真";word_translate[612]="贯";word_translate[613]="彻";word_translate[614]="针";
    word_translate[615]="策";word_translate[616]="终";word_translate[617]="身";word_translate[618]="素";
    word_translate[619]="养";word_translate[620]="风";word_translate[621]="军";word_translate[622]="核";
    word_translate[623]="价";word_translate[624]="值";word_translate[625]="积";word_translate[626]="念";
    word_translate[627]="忠";word_translate[628]="诚";word_translate[629]="履";word_translate[630]="等";
    word_translate[631]="教";word_translate[632]="育";word_translate[633]="记";word_translate[634]="录";
    word_translate[635]="笔";word_translate[636]="活";word_translate[637]="撰";word_translate[638]="_space";
    word_translate[639]="验";word_translate[640]="岗";word_translate[641]="先";word_translate[642]="诺";
    word_translate[643]="神";word_translate[644]="哨";word_translate[645]="尖";word_translate[646]="列";
    word_translate[647]="兵";word_translate[648]="斗";word_translate[649]="次";word_translate[650]="担";
    word_translate[651]="总";word_translate[652]="每";word_translate[653]="都";word_translate[654]="做";
    word_translate[655]="圆";word_translate[656]="满";word_translate[657]="常";word_translate[658]="仔";
    word_translate[659]="细";word_translate[660]="班";word_translate[661]="累";word_translate[662]="近";
    word_translate[663]="圈";word_translate[664]="识";word_translate[665]="懂";word_translate[666]="虚";
    word_translate[667]="向";word_translate[668]="周";word_translate[669]="围";word_translate[670]="紧";
    word_translate[671]="贴";word_translate[672]="础";word_translate[673]="促";word_translate[674]="较";
    word_translate[675]="短";word_translate[676]="掌";word_translate[677]="握";word_translate[678]="原";
    word_translate[679]="规";word_translate[680]="契";word_translate[681]="厂";word_translate[682]="里";
    word_translate[683]="收";word_translate[684]="资";word_translate[685]="料";word_translate[686]="万";
    word_translate[687]="字";word_translate[688]="践";word_translate[689]="出";word_translate[690]="打";
    word_translate[691]="故";word_translate[692]="障";word_translate[693]="落";word_translate[694]="划";
    word_translate[695]="隐";word_translate[696]="患";word_translate[697]="排";word_translate[698]="限";
    word_translate[699]="降";word_translate[700]="低";word_translate[701]="影";word_translate[702]="响";
    word_translate[703]="隙";word_translate[704]="休";word_translate[705]="反";word_translate[706]="功";
    word_translate[707]="傅";word_translate[708]="师";word_translate[709]="第";word_translate[710]="手";
    word_translate[711]="跳";word_translate[712]="双";word_translate[713]="捕";word_translate[714]="异";
    word_translate[715]="换";word_translate[716]="余";word_translate[717]="缺";word_translate[718]="失";
    word_translate[719]="返";word_translate[720]="翻";word_translate[721]="纸";word_translate[722]="板";
    word_translate[723]="转";word_translate[724]="文";word_translate[725]="片";word_translate[726]="芯";
    word_translate[727]="案";word_translate[728]="冗";word_translate[729]="脚";word_translate[730]="替";
    word_translate[731]="顺";word_translate[732]="执";word_translate[733]="气";word_translate[734]="折";
    word_translate[735]="射";word_translate[736]="段";word_translate[737]="待";word_translate[738]="析";
    word_translate[739]="届";word_translate[740]="奖";word_translate[741]="领";word_translate[742]="导";
    word_translate[743]="率";word_translate[744]="元";word_translate[745]="锁";word_translate[746]="取";
    word_translate[747]="绩";word_translate[748]="但";word_translate[749]="差";word_translate[750]="努";
    word_translate[751]="夯";word_translate[752]="拓";word_translate[753]="姿";word_translate[754]="饱";
    word_translate[755]="投";word_translate[756]="热";word_translate[757]="钻";word_translate[758]="精";
    word_translate[759]="创";word_translate[760]="突";word_translate[761]="破";word_translate[762]="争";
    word_translate[763]="几";word_translate[764]="篇";word_translate[765]="简";word_translate[766]="质";
    word_translate[767]="批";word_translate[768]="繁";word_translate[769]="忙";word_translate[770]="觉";
    word_translate[771]="即";word_translate[772]="滴";word_translate[773]="感";word_translate[774]="生";
    word_translate[775]="足";word_translate[776]="言";word_translate[777]="忘";word_translate[778]="只";
    word_translate[779]="训";word_translate[780]="存";word_translate[781]="源";word_translate[782]="径";
    word_translate[783]="才";word_translate[784]="未";word_translate[785]="今";word_translate[786]="布";
    word_translate[787]="密";word_translate[788]="条";word_translate[789]="听";word_translate[790]="讲";
    word_translate[791]="阅";word_translate[792]="特";word_translate[793]="层";word_translate[794]="雷";
    word_translate[795]="锋";word_translate[796]="郭";word_translate[797]="迹";word_translate[798]="触";
    word_translate[799]="颇";word_translate[800]="那";word_translate[801]="兢";word_translate[802]="献";
    word_translate[803]="血";word_translate[804]="贫";word_translate[805]="困";word_translate[806]="私";
    word_translate[807]="清";word_translate[808]="楚";word_translate[809]="古";word_translate[810]="彼";
    word_translate[811]="百";word_translate[812]="战";word_translate[813]="殆";word_translate[814]="飞";
    word_translate[815]="谁";word_translate[816]="秘";word_translate[817]="刃";word_translate[818]="权";
    word_translate[819]="而";word_translate[820]="胜";word_translate[821]="尤";word_translate[822]="刻";
    word_translate[823]="醒";word_translate[824]="警";word_translate[825]="钟";word_translate[826]="鸣";
    word_translate[827]="防";word_translate[828]="燃";word_translate[829]="练";word_translate[830]="且";
    word_translate[831]="没";word_translate[832]="少";word_translate[833]="给";word_translate[834]="遵";
    word_translate[835]="守";word_translate[836]="严";word_translate[837]="章";word_translate[838]="考";/**/
    word_translate[839]="熟";word_translate[840]="适";word_translate[841]="牢";word_translate[842]="悉";
    word_translate[843]="格";word_translate[844]="敢";word_translate[845]="旁";word_translate[846]="边";
    word_translate[847]="达";word_translate[848]="耳";word_translate[849]="颗";word_translate[850]="疾";
    word_translate[851]="病";word_translate[852]="痛";word_translate[853]="苦";word_translate[854]="编";
    word_translate[855]="演";word_translate[856]="急";word_translate[857]="书";word_translate[858]="爬";
    word_translate[859]="借";word_translate[860]="晚";word_translate[861]="末";word_translate[862]="承";
    word_translate[863]="初";word_translate[864]="智";word_translate[865]="双字节减号";word_translate[866]="销";
    word_translate[867]="假";word_translate[868]="依";word_translate[869]="早";word_translate[870]="意";
    word_translate[871]="象";word_translate[872]="轻";word_translate[873]="传";word_translate[874]="扩";
    word_translate[875]="频";word_translate[876]="外";word_translate[877]="双字节分号";word_translate[878]="欠";
    word_translate[879]="躁";word_translate[880]="绪";word_translate[881]="穿";word_translate[882]="卫";
    word_translate[883]="星";word_translate[884]="扰";word_translate[885]="首";word_translate[886]="双字节感叹号";
    word_translate[887]="半";word_translate[888]="武";word_translate[889]="脑";word_translate[890]="胡";
    word_translate[891]="难";word_translate[892]="准";word_translate[893]="顾";word_translate[894]="则";
    word_translate[895]="监";word_translate[896]="骨";word_translate[897]="队";word_translate[898]="杨";
    word_translate[899]="海";word_translate[900]="龙";word_translate[901]="男";word_translate[902]="汉";
    word_translate[903]="陕";word_translate[904]="乾";word_translate[905]="县";word_translate[906]="吉";
    word_translate[907]="林";word_translate[908]="嘉";word_translate[909]="拥";word_translate[910]="央";
    word_translate[911]="照";word_translate[912]="宗";word_translate[913]="旨";word_translate[914]="培";
    word_translate[915]="尚";word_translate[916]="德";word_translate[917]="健";word_translate[918]="趣";
    word_translate[919]="门";word_translate[920]="劳";word_translate[921]="怨";word_translate[922]="凡";
    word_translate[923]="台";word_translate[924]="烧";word_translate[925]="毁";word_translate[926]="晶";
    word_translate[927]="振";word_translate[928]="帧";word_translate[929]="遥";word_translate[930]="比";
    word_translate[931]="烽";word_translate[932]="火";word_translate[933]="施";word_translate[934]="希";
    word_translate[935]="望";word_translate[936]="耗";word_translate[937]="巡";word_translate[938]="公";
    word_translate[939]="措";word_translate[940]="畏";word_translate[941]="虽";word_translate[942]="偏";
    word_translate[943]="薄";word_translate[944]="弱";word_translate[945]="唐";word_translate[946]="倾";
    word_translate[947]="籍";word_translate[948]="善";word_translate[949]="白";word_translate[950]="刊";
    word_translate[951]="综";word_translate[952]="吴";word_translate[953]="扬";word_translate[954]="乏";
    word_translate[955]="材";word_translate[956]="搞";word_translate[957]="刚";word_translate[958]="候";
    word_translate[959]="恰";word_translate[960]="逢";word_translate[961]="怕";word_translate[962]="逐";
    word_translate[963]="渐";word_translate[964]="宝";word_translate[965]="贵";word_translate[966]="底";
    word_translate[967]="督";word_translate[968]="妥";word_translate[969]="移";word_translate[970]="载";
    word_translate[971]="登";word_translate[972]="谣";word_translate[973]="惕";word_translate[974]="松";
    word_translate[975]="互";word_translate[976]="娱";word_translate[977]="乐";word_translate[978]="两";
    word_translate[979]="角";word_translate[980]="摘";word_translate[981]="双字节左中括号";word_translate[982]="双字节右中括号";
    word_translate[983]="沿";word_translate[984]="椭";word_translate[985]="引";word_translate[986]="迭";
    word_translate[987]="讨";word_translate[988]="推";word_translate[989]="采";word_translate[990]="顿";
    word_translate[991]="拟";word_translate[992]="典";word_translate[993]="敛";word_translate[994]="阶";
    word_translate[995]="慢";word_translate[996]="消";word_translate[997]="宙";word_translate[998]="渔";
    word_translate[999]="舟";word_translate[1000]="石";word_translate[1001]="雨";word_translate[1002]="豆";
    word_translate[1003]="浆";word_translate[1004]="阻";word_translate[1005]="塞";word_translate[1006]="另";
    word_translate[1007]="疑";word_translate[1008]="描";word_translate[1009]="轮";word_translate[1010]="厘";
    word_translate[1011]="吏";word_translate[1012]="哩";word_translate[1013]="犁";word_translate[1014]="黎";
    word_translate[1015]="篱";word_translate[1016]="厉";word_translate[1017]="莉";word_translate[1018]="砾";
    word_translate[1019]="鹂";word_translate[1020]="呵";word_translate[1021]="哈";word_translate[1022]="柱";
    word_translate[1023]="席";word_translate[1024]="梦";word_translate[1025]="富";word_translate[1026]="英";
    word_translate[1027]="哲";word_translate[1028]="曾";word_translate[1029]="诗";word_translate[1030]="灵";
    word_translate[1031]="透";word_translate[1032]="物";word_translate[1033]="沉";word_translate[1034]="伦";
    word_translate[1035]="庄";word_translate[1036]="逻";word_translate[1037]="辑";word_translate[1038]="辞";
    word_translate[1039]="辩";word_translate[1040]="单字节双引号";word_translate[1041]="句";word_translate[1042]="鉴";
    word_translate[1043]="鲜";word_translate[1044]="绎";word_translate[1045]="块";word_translate[1046]="再";
    word_translate[1047]="咱";word_translate[1048]="辛";word_translate[1049]="铭";word_translate[1050]="介";
    word_translate[1051]="绍";word_translate[1052]="托";word_translate[1053]="着";word_translate[1054]="雄";
    word_translate[1055]="树";word_translate[1056]="榜";word_translate[1057]="官";word_translate[1058]="窗";
    word_translate[1059]="荣";word_translate[1060]="艺";word_translate[1061]="双字节三个点";word_translate[1062]="避";
    word_translate[1063]="蕴";word_translate[1064]="含";word_translate[1065]="觑";word_translate[1066]="眼";
    word_translate[1067]="晓";word_translate[1068]="歌";word_translate[1069]="懈";word_translate[1070]="易";
    word_translate[1071]="矣";word_translate[1072]="亿";word_translate[1073]="毅";word_translate[1074]="译";
    word_translate[1075]="闷";word_translate[1076]="扪";word_translate[1077]="冠";word_translate[1078]="惯";
    word_translate[1079]="灌";word_translate[1080]="罐";word_translate[1081]="馆";word_translate[1082]="纳";
    word_translate[1083]="棺";word_translate[1084]="仑";word_translate[1085]="纶";word_translate[1086]="囵";
    word_translate[1087]="沦";word_translate[1088]="柯";word_translate[1089]="棵";word_translate[1090]="客";
    word_translate[1091]="课";word_translate[1092]="壳";word_translate[1093]="园";word_translate[1094]="春";
    word_translate[1095]="湖";word_translate[1096]="硫";word_translate[1097]="黄";word_translate[1098]="皇";
    word_translate[1099]="刘";word_translate[1100]="邓";word_translate[1101]="恩";word_translate[1102]="京";
    word_translate[1103]="幻";word_translate[1104]="稀";word_translate[1105]="毛";word_translate[1106]="泽";
    word_translate[1107]="妈";word_translate[1108]="爸";word_translate[1109]="祖";word_translate[1110]="岁";
    word_translate[1111]="宁";word_translate[1112]="斯";word_translate[1113]="剩";word_translate[1114]="纲";
    word_translate[1115]="润";word_translate[1116]="筑";word_translate[1117]="尔";word_translate[1118]="儿";
    word_translate[1119]="贰";word_translate[1120]="饵";word_translate[1121]="诱";word_translate[1122]="腐";
    word_translate[1123]="倡";word_translate[1124]="廉";word_translate[1125]="墙";word_translate[1126]="抢";
    word_translate[1127]="枪";word_translate[1128]="腔";word_translate[1129]="羌";word_translate[1130]="蔷";
    word_translate[1131]="铿";word_translate[1132]="锵";word_translate[1133]="玫";word_translate[1134]="瑰";
    word_translate[1135]="孙";word_translate[1136]="山";word_translate[1137]="爷";word_translate[1138]="奶";
    word_translate[1139]="婆";word_translate[1140]="魂";word_translate[1141]="糖";word_translate[1142]="衣";
    word_translate[1143]="炮";word_translate[1144]="弹";word_translate[1145]="贪";word_translate[1146]="污";
    word_translate[1147]="味";word_translate[1148]="福";word_translate[1149]="幸";word_translate[1150]="伍";
    word_translate[1151]="磨";word_translate[1152]="灭";word_translate[1153]="贡";word_translate[1154]="辈";
    word_translate[1155]="默";word_translate[1156]="把";word_translate[1157]="崇";word_translate[1158]="壮";
    word_translate[1159]="烈";word_translate[1160]="浓";word_translate[1161]="厚";word_translate[1162]="奉";
    word_translate[1163]="仰";word_translate[1164]="谋";word_translate[1165]="拉";word_translate[1166]="迅";
    word_translate[1167]="夺";word_translate[1168]="秒";word_translate[1169]="又";word_translate[1170]="箭";
    word_translate[1171]="挑";word_translate[1172]="世";word_translate[1173]="界";word_translate[1174]="牌";
    word_translate[1175]="举";word_translate[1176]="瞩";word_translate[1177]="激";word_translate[1178]="螺";
    word_translate[1179]="丝";word_translate[1180]="钉";word_translate[1181]="昼";word_translate[1182]="夜";
    word_translate[1183]="船";word_translate[1184]="驾";word_translate[1185]="企";word_translate[1186]="么";
    word_translate[1187]="滋";word_translate[1188]="却";word_translate[1189]="豪";word_translate[1190]="江";
    word_translate[1191]="察";word_translate[1192]="词";word_translate[1193]="劲";word_translate[1194]="辜";
    word_translate[1195]="继";word_translate[1196]="续";word_translate[1197]="妙";word_translate[1198]="辉";
       word_translate[1199]="旭";word_translate[1120]="叙";word_translate[1121]="徐";word_translate[1122]="恤";
    word_translate[1123]="嘘";word_translate[1124]="蓄";word_translate[1125]="喜";word_translate[1126]="怒";
    word_translate[1127]="哀";word_translate[1128]="洗";word_translate[1129]="吸";word_translate[1130]="兮";
    word_translate[1131]="溪";












    //GetDlgItem(IDC_EDIT1)->GetWindowText(str);
    str = "1234567890";//此处str为真实文本内容
    str = noteStr;
   // QByteArray charList;
   // charList = str.toLocal8Bit();
    str_len=str.size();
    //str_len =  charList.size();
    qDebug()<<"str_len"<<str_len;
    QString endStr="\n";
    if(str_len <=0)
    {
        ;
    }else
    {
        rec_len=0;
        find_sigle=0;
        find_double=0;
        now_insert_pos_x=0;
        now_insert_pos_y=0;

        for(i=0;i<str_len; i=i+1)//i 起到控制总宽度，和当前插入图片的位置的做用
        {
           // str_temp.Format("%c",str[i]);

            str_temp = str[i];
            rec_len = str_temp.toLocal8Bit().size();
            if(str_temp == endStr) // 判断如果是换行符则另起新行
            {
                now_insert_pos_x=0;
                now_insert_pos_y=now_insert_pos_y+1;
                if(now_insert_pos_y >= max_line_in_big_picture )//如果这页满了，起用新的一页
                {
                    now_insert_pos_y=0;
                    big_picture_numeric=big_picture_numeric+1;
                   // creat_file.Format("%d",big_picture_numeric);
                    creat_file = QString::number(big_picture_numeric);
                    creat_file="big_old_"+creat_file+".bmp";
                }
                continue;
            }

            find_sigle=0;
            find_double=0;

            for(i2=0;i2<2000;i2++)
            {    str_for_save=word_ku[i2];
                 //str_cmp=str_temp.Compare(str_for_save);
                if(str_temp == str_for_save )
                {
                    str_cmp=0; //same
                }else
                {
                    str_cmp=1; //not same
                }



                if((str_cmp==0) && (str_temp.size()>0) &&  (str_for_save.size()>0))
                {
                   insert_file=word_translate[i2]+".bmp"+'\0';
                   //rec_len=1;
                   if((now_insert_pos_x*word_width)>(big_picture_width-rec_len*word_width))
                   {
                       now_insert_pos_x=0;
                       now_insert_pos_y=now_insert_pos_y+1;
                       if(now_insert_pos_y >= max_line_in_big_picture )
                       {
                           now_insert_pos_y=0;
                           big_picture_numeric=big_picture_numeric+1;
                          // creat_file.Format("%d",big_picture_numeric);
                           creat_file = QString::number(big_picture_numeric);
                           creat_file="big_old_"+creat_file+".bmp";
                       }

                    }

                   if(now_insert_pos_x==0 && now_insert_pos_y==0)
                   {
                       Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width_no_fix,infact_pos_y[now_insert_pos_y],creat_file,insert_file,file_name_last);
                   }else if(now_insert_pos_x !=0 && now_insert_pos_y !=0)
                   {
                       Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width,infact_pos_y[now_insert_pos_y],creat_file,insert_file,creat_file);
                   }else if(now_insert_pos_x !=0 && now_insert_pos_y ==0)
                   {
                       Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width,infact_pos_y[now_insert_pos_y],creat_file,insert_file,creat_file);
                   }else if(now_insert_pos_x ==0 && now_insert_pos_y !=0)
                   {
                       Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width_no_fix,infact_pos_y[now_insert_pos_y],creat_file,insert_file,creat_file);
                   }



                  // now_insert_pos_x=now_insert_pos_x+rec_len*2;//vc6 与 qt的字符在处理中文时有所不同
                   now_insert_pos_x=now_insert_pos_x+rec_len;
                            if((now_insert_pos_x*word_width)>(big_picture_width-rec_len*word_width))
                            {
                                now_insert_pos_x=0;
                                now_insert_pos_y=now_insert_pos_y+1;
                                if(now_insert_pos_y >= max_line_in_big_picture )
                                {
                                    now_insert_pos_y=0;
                                    big_picture_numeric=big_picture_numeric+1;
                                   // creat_file.Format("%d",big_picture_numeric);
                                    creat_file = QString::number(big_picture_numeric);
                                    creat_file="big_old_"+creat_file+".bmp";
                                }

                            }
                   find_sigle=1;
                   break;
                }else
                {
                    find_sigle=0;

                }
            }
            //

            //此处长度不对，应根据长度判读加1个？还是2个？
            if( find_sigle==0)
            {
                insert_file="unkown.bmp";
                rec_len=1;
                if((now_insert_pos_x*word_width)>(big_picture_width-rec_len*word_width))
                {
                    now_insert_pos_x=0;
                    now_insert_pos_y=now_insert_pos_y+1;
                    if(now_insert_pos_y >= max_line_in_big_picture )
                    {
                        now_insert_pos_y=0;
                        big_picture_numeric=big_picture_numeric+1;
                      //  creat_file.Format("%d",big_picture_numeric);
                        creat_file = QString::number(big_picture_numeric);
                        creat_file="big_old_"+creat_file+".bmp";
                    }

                }

                if(now_insert_pos_x==0 && now_insert_pos_y==0)
                {
                    Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width_no_fix,infact_pos_y[now_insert_pos_y],creat_file,insert_file,file_name_last);
                }else if(now_insert_pos_x !=0 && now_insert_pos_y !=0)
                {
                    Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width,infact_pos_y[now_insert_pos_y],creat_file,insert_file,creat_file);
                }else if(now_insert_pos_x !=0 && now_insert_pos_y ==0)
                {
                    Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width,infact_pos_y[now_insert_pos_y],creat_file,insert_file,creat_file);
                }else if(now_insert_pos_x ==0 && now_insert_pos_y !=0)
                {
                    Insert_small_bmp_to_big_bmp(now_insert_pos_x*word_width_no_fix,infact_pos_y[now_insert_pos_y],creat_file,insert_file,creat_file);
                }


                now_insert_pos_x=now_insert_pos_x+rec_len;//多页生成
//                             if((now_insert_pos_x*word_width)>(big_picture_width-rec_len*word_width))
//                             {
//                                 now_insert_pos_x=0;
//                                 now_insert_pos_y=now_insert_pos_y+1;
//                                 if(now_insert_pos_y >= max_line_in_big_picture )
//                                 {
//                                     now_insert_pos_y=0;
//                                     big_picture_numeric=big_picture_numeric+1;
//                                     creat_file.Format("%d",big_picture_numeric);
//                                     creat_file="big_old_"+creat_file+".bmp";
//                                 }
//
//                             }

            }
            //

        }
        ;
    }

}
